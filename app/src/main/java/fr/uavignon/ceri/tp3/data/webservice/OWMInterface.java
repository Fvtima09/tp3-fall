package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OWMInterface {

    //API 36504e0ba1cb3c5cdff5de386a2fa724
    @Headers("Accept: application/geo+json")
    @GET("/data/2.5/weather?APIkey=36504e0ba1cb3c5cdff5de386a2fa724")
    Call<WeatherResponse> getForecast(@Query("q") String q,
                                      @Query("apikey") String apikey);
}
