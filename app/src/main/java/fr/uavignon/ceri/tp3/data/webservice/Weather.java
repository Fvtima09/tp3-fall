package fr.uavignon.ceri.tp3.data.webservice;

public class Weather {

    public final String icon;
    public final String description;

    public Weather(String icon, String description) {
        this.icon = icon;
        this.description = description;
    }
}
