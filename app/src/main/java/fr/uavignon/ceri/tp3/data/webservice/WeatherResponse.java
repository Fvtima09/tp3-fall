package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {

    public final List<Weather> weather = null;

    public final Main main = null;
    public final Wind wind = null;
    public  final Cloud clouds = null;


    public static class Main {
        public final Float temp;
        public final Integer humidity;

        public Main(Float temp, Integer humidity) {
            this.temp = temp;
            this.humidity = humidity;
        }
    }

    public static class Wind {
        public final Float speed;
        public final Integer deg;

        public Wind(Float speed, Integer deg) {
            this.speed = speed;
            this.deg = deg;
        }
    }

    public static  class Cloud {
        public final Integer all;

        public Cloud(Integer all) {
            this.all = all;
        }
    }
}