package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    private CityDao cityDao;
    private OWMInterface api;

    private static volatile WeatherRepository INSTANCE;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);

        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getSynchrAllCities();
        selectedCity = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
    }

    public void loadWeatherCity(City city, int index) {

        final MutableLiveData<WeatherResult> result = new MutableLiveData<>();
        isLoading.postValue(true);

        String requete = city.getName()+",,"+city.getCountryCode();
        String apikey = "36504e0ba1cb3c5cdff5de386a2fa724";

        api.getForecast(requete, apikey).enqueue(
                new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call,
                                           Response<WeatherResponse> response) {
                        result.postValue(new WeatherResult(false,  response.body().weather,response.body().main,response.body().wind,response.body().clouds, null));
                        result.getValue().transferInfo(response.body(), city);
                        updateCity(city);
                        if(index>=1){
                            isLoading.postValue(true);
                        }
                        else{
                            isLoading.postValue(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        result.postValue(new WeatherResult(false, null, null, null, null, null));
                        webServiceThrowable.postValue(t);
                        if(index>=1){
                            isLoading.postValue(true);
                        }
                        else{
                            isLoading.postValue(false);
                        }
                    }
                });
    }




    public void loadWeatherAllCities(){
        List<City> all = allCities.getValue();
        int index = all.size()-1;
        if(all!=null){
            for (City city : all) {
                index--;
                loadWeatherCity(city, index);
            }
        }
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }

    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city) {
        Future<Integer> futurint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = futurint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
