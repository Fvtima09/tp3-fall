package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {


    public final List<Weather> weathers;
    public final WeatherResponse.Main main;
    public final WeatherResponse.Wind wind;
    public final WeatherResponse.Cloud clouds;

    public final boolean isLoading;
    public final Throwable error;

    public WeatherResult(boolean isLoading, List<Weather> weathers, WeatherResponse.Main main, WeatherResponse.Wind wind, WeatherResponse.Cloud clouds, Throwable error) {
        this.isLoading = isLoading;
        this.main = main;
        this.wind = wind;
        this.clouds = clouds;
        this.weathers = weathers;
        this.error = error;
    }

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo) {

        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeed(Math.round(weatherInfo.wind.speed));
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setLastUpdate(System.currentTimeMillis()/1000L);
    }

}
